<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PhoneBookController@showRecords')->name('home');

Route::post('/save', 'PhoneBookController@saveRecord');

Route::post('/update', 'PhoneBookController@updateRecord');

Route::get('/delete', 'PhoneBookController@deleteRecord');