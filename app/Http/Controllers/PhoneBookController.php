<?php

namespace App\Http\Controllers;

use App\Record;
use Illuminate\Http\Request;

class PhoneBookController extends Controller
{

    public function showRecords(Request $request)
    {
        $action = $request->get('action');
        $id = $request->get('id');
        $record = Record::find($id);
        $records = Record::all()->reverse();
        if (!isset($action)) {
            $action = 'save';
        }
        return view('homepage', array('action' => $action, 'records' => $records, 'record' => $record));
    }

    public function updateRecord(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'number' => 'required|numeric'
        ]);
        $id = $request->get('record_id');
        $name = $request->get('name');
        $number = $request->get('number');
        $this->update($id, $name, $number);
        return redirect()->route('home');
    }

    public function deleteRecord(Request $request)
    {
        $id = $request->get('id');
        $record = Record::find($id);
        $record->delete();
        return redirect()->route('home');
    }

    public function saveRecord(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'number' => 'required|numeric'
        ]);
        $name = $request->get('name');
        $number = $request->get('number');
        $this->save($name, $number);
        return redirect()->route('home');
    }

    private function save($name, $number)
    {
        $record = Record::create(array(
            'name' => $name,
            'phoneNumber' => $number
        ));
        $record->save();
    }

    private function update($id, $name, $number)
    {
        $record = Record::find($id);
        $record->name = $name;
        $record->phoneNumber = $number;
        $record->save();
    }
}