<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style>
        .addcontact {
            margin-top: 50px;
        }

        .list {
            padding-top: 60px;
        }
    </style>
    <title>Phonebook</title>
</head>
<body>

<div class="container">
    <h1><a href="/">Phonebook.</a></h1>

    <div class="addcontact">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="form-inline" method="post" action="/{{ $action }}">
            {{ csrf_field() }}
            <input type="hidden" name="action" value="{{ $action }}">
            <input type="hidden" name="record_id" value="{{ isset($record) ? $record->id : '' }}">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="number">Phone number</label>
                    <input name="number" type="text" class="form-control" id="number"
                           value="{{ isset($record) ? $record->phoneNumber : ''}}">
                </div>
                <div class="form-group">

                    <label for="name">Name</label>
                    <input name="name" type="text" class="form-control" id="name"
                           value="{{isset($record) ? $record->name : ''}}">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success btn-sm" value="{{ $action }}">
                </div>

            </div>
        </form>
    </div>
    <div class="list">
        <div class="row">
            <div class="col-sm-6">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Phone number</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($records as $record)
                        <tr>
                            <td class="col-sm-2">{{$record->phoneNumber}}</td>
                            <td class="col-sm-2">{{$record->name}}</td>
                            <td class="col-sm-1">
                                <a class="btn btn-primary btn-sm" href="/?action=update&id={{$record->id}}">Edit</a>
                                <a class="btn btn-danger btn-sm" href="/delete?id={{$record->id}}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


</body>
</html>